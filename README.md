## CoderForum

git追加：

```git
git add .

git commit --amend --no-edit

git push
```

##FLYWAY 用于数据库版本控制
删库(控制台中输入)
```bash
rm ~community.*
```
执行migration中的sql语句
```bash
mvn flyway:migrate
```
通过flyway_schema_history中的记录来确定数据库版本和已经执行过的sql语句

##h2数据库是什么 和 链接异常处理
###1.使用h2数据库的优点
1. 直接在项目中导入jar包就可以启动项目，并且有数据库的链接功能，减少了搭建服务和配置的过程
###2.h2数据库的三种启动方式
1. 通过命令直接启动jar包。充当类似于MySQL的服务去启动，在这种情况下支持多个连接进行数据库的访问，即不需要操作数据库时将项目关闭然后再连接数据库进行访问。这种方式也带来了一个问题，在每次启动项目时，需要手动启动这个服务
2. 通过内存的方式。在这种方式中，每次启动项目时自动启动数据库，在项目关闭的时候数据库也随之消失。这种启动方式无法进行调试
3. 以数据库文件的方式进行存储启动。因为一个文件只允许一个进程打开，因此导致只能进行单个连接。文件application.properties中配置了spring.datasource.url地址，这个地址下存的就是数据库文件

###Mybatis Generator
```bash
    mvn -Dmybatis.generator.overwrite=true mybatis-generator:generate
```


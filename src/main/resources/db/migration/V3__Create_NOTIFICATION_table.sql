create table NOTIFICATION
(
    ID            BIGINT auto_increment,
    NOTIFIER      BIGINT        not null,
    RECEIVER      BIGINT        not null,
    OUTER_ID      BIGINT        not null,
    TYPE          INT           not null,
    GMT_CREATE    BIGINT        not null,
    STATUS        INT default 0 not null,
    NOTIFIER_NAME VARCHAR(100),
    OUTER_TITLE   VARCHAR(256),
    constraint NOTIFICATION_PK
        primary key (ID)
);
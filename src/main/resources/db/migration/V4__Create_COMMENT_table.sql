create table COMMENT
(
    ID            BIGINT auto_increment,
    PARENT_ID     BIGINT not null,
    TYPE          INT    not null,
    COMMENTATOR   BIGINT not null,
    GMT_CREATE    BIGINT not null,
    GMT_MODIFIED  BIGINT not null,
    LIKE_COUNT    BIGINT default 0,
    CONTENT       VARCHAR(1024),
    COMMENT_COUNT BIGINT default 0,
    constraint COMMENT_PK
        primary key (ID)
);

comment on column COMMENT.PARENT_ID is '父类id';
comment on column COMMENT.TYPE is '父类类型';
comment on column COMMENT.GMT_CREATE is '创建时间';
comment on column COMMENT.GMT_MODIFIED is '修改时间';
comment on column COMMENT.LIKE_COUNT is '点赞数';
/**
 * 提交回复
 */
function post() {
    var questionId = $("#question_id").val();
    var content = $("#comment_content").val();

    commentToTarget(questionId, 1, content);
}

function commentToTarget(targetId, type, content) {

    //通过前端校验输入的评论内容是否为空，让用户尽可能块的获得信息
    if (!content) {
        alert("你当前还没进行评论,不能回复空内容哟～");
        return;
    }

    $.ajax({
        type: "POST",
        url: "/comment",
        contentType: "application/json",
        data: JSON.stringify({
            "parentId": targetId,
            "content": content,
            "type": type
        }),
        success: function (response) {
            if (response.code === 200) {
                window.location.reload();
            } else {
                if (response.code === 2003) {
                    //询问是否要进行登录
                    var isAccepted = confirm(response.message);
                    //如果要进行登录则跳转授权登录页面
                    if (isAccepted) {
                        window.open("https://gitee.com/oauth/authorize?client_id=8ae12e5d5cc318c7f2c7c9f6cfa3c123a615c398952909c6f080231390368983&redirect_uri=http://localhost:8888/callback&response_type=code&state=1");
                        window.localStorage.setItem("closable", true);
                    }
                } else {
                    //未登录时弹出警告框提示未登录
                    alert(response.message);
                }
            }
        },
        dataType: "json"
    });
}

function comment(e) {
    var commentId = e.getAttribute("data-id");
    var content = $("#input-" + commentId).val();
    commentToTarget(commentId, 2, content);
}

/**
 * 展开与折叠二级评论
 */
function collapseComments(e) {
    var id = e.getAttribute("data-id");
    var comment = $("#comment-" + id);

    //获取二级评论的展开状态
    var collapse = e.getAttribute("data-collapse");
    //根据展开状态选择折叠与展开二级评论
    if (collapse) {
        //折叠二级评论
        comment.removeClass("in");
        //标记评论展开状态
        e.removeAttribute("data-collapse");
        //折叠情况下不高亮显示图标
        e.classList.remove("active");
    } else {
        var subCommentContainer = $("#comment-" + id);
        //判断二级评论中是否存在评论的数据，如果存在则直接加载，而不是查询
        if (subCommentContainer.children().length !== 1) {
            //展开二级评论
            comment.addClass("in");
            //标记评论展开状态
            e.setAttribute("data-collapse", "in");
            //展开情况下高亮显示图标
            e.classList.add("active");
        } else {
            $.getJSON("/comment/" + id, function (data) {
                $.each(data.data.reverse(), function (index, comment) {

                    var mediaLeftElement = $("<div/>", {
                        "class": "media-left"
                    }).append($("<img/>", {
                        "class": "media-object img-rounded",
                        "src": comment.user.avatarUrl
                    }));

                    var mediaBodyElement = $("<div/>", {
                        "class": "media-body"
                    }).append($("<h5/>", {
                        "class": "media-heading",
                        "html": comment.user.name
                    })).append($("<div/>", {
                        "html": comment.content
                    })).append($("<div/>", {
                        "class": "menu"
                    }).append($("<span/>", {
                        "class": "pull-right",
                        "html": moment(comment.gmtCreate).format('YYYY-MM-DD HH时mm分')
                    })));

                    var mediaElement = $("<div/>", {
                        "class": "media"
                    }).append(mediaLeftElement).append(mediaBodyElement);

                    var commentElement = $("<div/>", {
                        "class": "col-lg-12 col-md-12 col-sm-12 col-xs-12 comments"
                    }).append(mediaElement);

                    subCommentContainer.prepend(commentElement);
                });
                //展开二级评论
                comment.addClass("in");
                //标记评论展开状态
                e.setAttribute("data-collapse", "in");
                //展开情况下高亮显示图标
                e.classList.add("active");
            });
        }
    }
}

/**
 * 选择标签提供的标签并将结果放入标签输入框中
 * @param value
 */
function selectTag(e) {
    var value = e.getAttribute("data-tag")
    var previous = $("#tag").val();

    if(previous.indexOf(value) === -1){
        if (previous) {
            $("#tag").val(previous + ',' + value);
        }else{
            $("#tag").val(value);
        }
    }
}

/**
 * 展示Tag备选栏
 */
function showSelectTag() {
    $("#select-tag").show()
}
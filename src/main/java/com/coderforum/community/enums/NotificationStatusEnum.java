package com.coderforum.community.enums;

/**
 * 通知状态的枚举类
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/6 9:06 下午
 */
public enum NotificationStatusEnum {
    /**
     * 通知未读状态
     */
    UNREAD(0),
    /**
     * 通知已读状态
     */
    READ(1)
    ;

    private int status;

    NotificationStatusEnum(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}

package com.coderforum.community.enums;

/**
 * 通知类型枚举类
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/6 8:55 下午
 */
public enum NotificationTypeEnum {
    /**
     * 针对问题回复的通知
     */
    REPLY_QUESTION(1,"回复了问题"),
    /**
     * 针对评论回复的通知
     */
    REPLY_COMMENT(2,"回复了评论"),
    ;

    private int type;
    private String name;

    NotificationTypeEnum(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    /**
     * 根据传入的type类型返回枚举类的名字
     * @param type 传入的type类型
     * @return 返回枚举类的中枚举量的名字
     */
    public static String nameOfType(int type){
        for (NotificationTypeEnum notificationTypeEnum : NotificationTypeEnum.values()) {
            if(notificationTypeEnum.getType() == type){
                return notificationTypeEnum.getName();
            }
        }
        return "";
    }
}

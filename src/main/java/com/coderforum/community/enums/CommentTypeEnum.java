package com.coderforum.community.enums;

/**
 * 评论类型枚举类
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/4/27 8:40 下午
 */
public enum CommentTypeEnum {
    /**
     * 问题类型枚举值
     */
    QUESTION(1),
    /**
     * 评论类型枚举值
     */
    COMMENT(2);

    final private Integer type;

    public Integer getType() {
        return type;
    }

    CommentTypeEnum(Integer type) {
        this.type = type;
    }

    /**
     * 判断传入的评论类型是否存在
     *
     * @param type 评论的类型（属于问题的评论->1 属于评论的评论->2）
     * @return 遍历完评论类型枚举类之后，如果类型存在返回true，否则返回false
     */
    public static boolean isExist(Integer type) {
        for (CommentTypeEnum commentTypeEnum : CommentTypeEnum.values()) {
            if (commentTypeEnum.getType().equals(type)) {
                return true;
            }
        }
        return false;
    }
}

package com.coderforum.community.mapper;

import com.coderforum.community.dto.QuestionQueryDTO;
import com.coderforum.community.model.Question;
import com.coderforum.community.model.QuestionExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

/**
 * 向问题表更新的Mapper，QuestionExtMapper.xml文件通过namespace自动路由到这个文件
 * 并且使用上述xml文件中的update id = incView的标签完成数据库的更新
 * @author xuexuehai
 */
public interface QuestionExtMapper {

    /**
     * 更新问题的阅读数
     * @param record 待更新问题的阅读数
     * @return 返回增加以后的评论数
     */
    int incView(Question record);

    /**
     * 跟新评论数
     * @param record 传入待更新的问题
     * @return 返回增加以后的评论数
     */
    int incCommentCount(Question record);

    /**
     * 查询相关
     * @param question 传入的问题
     * @return 返回list存有问题
     */
    List<Question> selectRelated(Question question);

    /**
     * 根据search搜索问题数量，返回查询结果的数量
     * @param questionQueryDTO 传入的查询对象，包含查询关键字和分页步长和页码
     * @return 返回数量
     */
    Integer countBySearch(QuestionQueryDTO questionQueryDTO);

    /**
     * 返回根据关键字搜索的问题列表
     * @param questionQueryDTO 传入的查询对象，包含查询关键字和分页步长和页码
     * @return 符合查询条件的问题列表
     */
    List<Question> selectBySearch(QuestionQueryDTO questionQueryDTO);
}
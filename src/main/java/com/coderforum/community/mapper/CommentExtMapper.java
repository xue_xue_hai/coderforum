package com.coderforum.community.mapper;

import com.coderforum.community.model.Comment;
import com.coderforum.community.model.CommentExample;
import com.coderforum.community.model.Question;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;

public interface CommentExtMapper {
    int incCommentCount(Comment comment);

    int incLikeCount(Comment comment);
}
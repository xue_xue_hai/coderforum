package com.coderforum.community.controller;

import com.coderforum.community.dto.CommentCreateDTO;
import com.coderforum.community.dto.CommentDTO;
import com.coderforum.community.dto.ResultDTO;
import com.coderforum.community.enums.CommentTypeEnum;
import com.coderforum.community.exception.CustomizeErrorCode;
import com.coderforum.community.model.Comment;
import com.coderforum.community.model.User;
import com.coderforum.community.service.CommentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 评论控制器
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/4/27 6:40 下午
 */
@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    /**
     * 获取问题的一级评论
     * @param commentCreateDTO 传入的数据对象
     * @param request 向服务端发送一个request请求，判断用户是否处于登录态
     * @return 返回状态代码和状态信息
     */
    @ResponseBody
    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public Object post(@RequestBody CommentCreateDTO commentCreateDTO,
                       HttpServletRequest request) {

        //通过session判断用户是否登录
        User user = (User) request.getSession().getAttribute("user");
        //用户未登录返回错误信息，并提示
        if (user == null) {
            return ResultDTO.errorOf(CustomizeErrorCode.NO_LOGIN);
        }

        //判断用户输入的评论是否为空，为了避免数据写入数据库，所以在前端验证过之后后端仍然要对评论内容进行非空判断
        if (commentCreateDTO == null || StringUtils.isBlank(commentCreateDTO.getContent())) {
            return ResultDTO.errorOf(CustomizeErrorCode.CONTENT_IS_EMPTY);
        }

        Comment comment = new Comment();
        comment.setParentId(commentCreateDTO.getParentId());
        comment.setContent(commentCreateDTO.getContent());
        comment.setType(commentCreateDTO.getType());
        comment.setGmtCreate(System.currentTimeMillis());
        comment.setGmtModified(System.currentTimeMillis());
        comment.setCommentator(user.getId());
        comment.setLikeCount(0L);
        comment.setCommentCount(0L);
        commentService.insert(comment,user);
        return ResultDTO.okOf();
    }

    /**
     * 控制一级评论列表和二级评论列表
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/comment/{id}", method = RequestMethod.GET)
    public ResultDTO<List<CommentDTO>> comments(@PathVariable(name = "id") Long id){
        List<CommentDTO> commentDTOS = commentService.listByTargetId(id, CommentTypeEnum.COMMENT);
        return ResultDTO.okOf(commentDTOS);
    }
}

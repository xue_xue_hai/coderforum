package com.coderforum.community.controller;

import com.coderforum.community.dto.CommentDTO;
import com.coderforum.community.dto.QuestionDTO;
import com.coderforum.community.enums.CommentTypeEnum;
import com.coderforum.community.service.CommentService;
import com.coderforum.community.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * 修改问题页面的控制器
 * @Author: xuehai.XUE
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/4/19 20:30
 */
@Controller
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @Autowired
    private CommentService commentService;

    /**
     * 通过指定id修改问题的控制器
     * @param id 指定问题的id
     * @param model 用于接收questionDTO的内容
     * @return 返回question页面
     */
    @GetMapping("/question/{id}")
    public String question(@PathVariable(name = "id") Long id, Model model) {

        //返回QuestionDTO的原因是Question对象是数据库的模型，属性较前者少了User的部分信息
        QuestionDTO questionDTO = questionService.getById(id);

        List<QuestionDTO> relatedQuestions = questionService.selectRelated(questionDTO);

        List<CommentDTO> comments =  commentService.listByTargetId(id, CommentTypeEnum.QUESTION);

        //累计阅读数量
        questionService.incView(id);
        model.addAttribute("question", questionDTO);
        model.addAttribute("comments", comments);
        model.addAttribute("relatedQuestions",relatedQuestions);

        return "question";
    }
}

package com.coderforum.community.exception;

/**
 * 自定义异常代码接口的实现类，其中定义了可能出现的异常类型，
 * 并对这些异常类型设置了异常代码和异常信息
 *
 * @author xuexuehai
 */

public enum CustomizeErrorCode implements CustomizeErrorCodeInterface {

    /**
     * 问题未找到
     */
    QUESTION_NOT_FOUND(2001, "你找的问题已被删除或下架，要不换个试试？"),
    /**
     * 问题或评论未选中直接评论
     */
    TARGET_PARAM_NOT_FOUND(2002, "未选中任何问题或评论进行回复！"),
    /**
     * 用户未登录
     */
    NO_LOGIN(2003, "当前用户未登录不能进行评论，请先登录～"),
    /**
     * 服务器异常
     */
    SYSTEM_ERROR(2004, "阁下手速太快了，服务器起飞啦，一会再试试吧。"),
    /**
     * question代码为1，comment代码为2，其他的报此错误
     */
    TYPE_PARAM_WRONG(2005, "评论类型错误，或不存在"),
    /**
     * 评论未找到
     */
    COMMENT_NOT_FOUND(2006, "你回复的评论不存在，要不换个试试？"),
    /**
     * 内容为空
     */
    CONTENT_IS_EMPTY(2007, "输入内容不能为空"),
    /**
     * 非用户本人不能消除未读评论
     */
    READ_NOTIFICATION_FAIL(2008, "你不能读取他人的未读信息"),
    /**
     * 当用户访问不存在的消息是，抛出此异常
     */
    NOTIFICATION_NOT_FOUND(2009, "你找的消息不见了，要不换一个试试？"),
    FILE_UPLOAD_FAIL(2010, "图片上传失败"),

    ;

    private String message;

    private Integer code;

    CustomizeErrorCode(Integer code, String message) {
        this.message = message;
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Integer getCode() {
        return code;
    }
}

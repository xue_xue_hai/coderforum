package com.coderforum.community.exception;

/**
 * 自定义异常代码返回信息的接口
 *
 * @author xuexuehai
 */
public interface CustomizeErrorCodeInterface {
    /**
     * 获取错误信息
     *
     * @return 返回错误信息
     */
    String getMessage();

    /**
     * 获取错误代码
     *
     * @return 返回错误代码
     */
    Integer getCode();
}

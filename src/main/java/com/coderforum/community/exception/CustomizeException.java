package com.coderforum.community.exception;

/**
 * 自定义异常类，继承于运行时异常
 * 包含code异常代码和message异常信息
 *
 * @author xuexuehai
 */
public class CustomizeException extends RuntimeException {
    private Integer code;
    private String message;

    /**
     * 自定义异常的构造方法
     *
     * @param errorCode 传入的异常代码errorCode
     */
    public CustomizeException(CustomizeErrorCodeInterface errorCode) {
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
    }

    /**
     * 重写运行时异常父类中的getMessage()方法
     *
     * @return 返回错误信息
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     * 获取异常的错误代码
     *
     * @return 返回异常代码
     */
    public Integer getCode() {
        return code;
    }
}

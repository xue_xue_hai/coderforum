package com.coderforum.community.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @Author: xuehai.XUE
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/4/1 20:56
 * @Description: 用于数据传输的对象AccessTokenDTO
 */
@Component
@Data
public class AccessTokenDTO {
    private String client_id;
    private String redirect_uri;
    private String client_secret;
    private String code;
    private String state;
}

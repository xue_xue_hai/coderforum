package com.coderforum.community.dto;

import lombok.Data;

/**
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/10 3:21 下午
 */
@Data
public class FileDTO {
    private Integer success;
    private String message;
    private String url;
}

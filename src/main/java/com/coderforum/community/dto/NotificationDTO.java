package com.coderforum.community.dto;

import com.coderforum.community.model.User;
import lombok.Data;

/**
 * 通知传输对象
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/6 9:35 下午
 */
@Data
public class NotificationDTO {
    private Long id;
    private Long gmtCreate;
    private Integer status;
    private Long notifier;
    private String notifierName;
    private Long outerId;
    private String outerTitle;
    private String typeName;
    private Integer type;
}

package com.coderforum.community.dto;

import lombok.Data;

/**
 * @Author: xuehai.XUE
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/4/2 13:33
 * @Description: 用于传回JSON格式的用户，解析为这个目标类
 */
@Data
public class GiteeUser {
    private String name;
    private Long id;
    private String bio;
    private String avatarUrl;
}

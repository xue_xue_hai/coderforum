package com.coderforum.community.dto;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xuehai.XUE
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/4/16 9:49
 * @Description: 该DTO对象封装了需要的前端样式和元素
 */
@Data
public class PaginationDTO<T> {
    /**
     * 当前高亮页前显示的页数
     */
    final int STEP_SIZE = 3;

    private List <T> data;
    private Boolean showPrevious;
    private Boolean showFirstPage;
    private Boolean showNext;
    private Boolean showEndPage;
    private Integer page;
    private List <Integer> pages = new ArrayList <>();
    private Integer totalPage;

    /**
     * 通过总数\当前页数\当前每页的显示数量 计算 是否展示上一页\下一页 和 跳转第一页\最后一页
     *
     * @param totalPage 传入的数据库中查询到的总数据量
     * @param page      页码
     */
    public void setPagination(Integer totalPage, Integer page) {
        this.totalPage = totalPage;
        this.page = page;

        //页码显示逻辑
        pages.add(page);
        for (int i = 1; i <= STEP_SIZE; i++) {
            //向前展示三个页码
            if (page - i > 0) {
                pages.add(0, page - i);
            }

            //向后展示三个页码
            if (page + i <= totalPage) {
                pages.add(page + i);
            }
        }

/*
1 1234567
2 1234567
3 1234567
4 1234567
5 2345678
*/

        //是否展示上一页按钮
        if (page == 1) {
            showPrevious = false;
        } else {
            showPrevious = true;
        }
        //是否展示下一页按钮
        if (page.equals(totalPage)) {
            showNext = false;
        } else {
            showNext = true;
        }

        //是否展示第一页按钮
        if (pages.contains(1)) {
            showFirstPage = false;
        } else {
            showFirstPage = true;
        }
        //是否展示末一页按钮
        if (pages.contains(totalPage)) {
            showEndPage = false;
        } else {
            showEndPage = true;
        }
    }
}

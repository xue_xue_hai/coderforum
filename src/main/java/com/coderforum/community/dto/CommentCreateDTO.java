package com.coderforum.community.dto;

import lombok.Data;

/**
 * 创建的评论数据传输对象，包含父Id、评论内容、评论所属类型，
 * 通过parentId来区分评论是属于哪一个问题的评论
 * 评论所属类型用于区分评论是属于问题的评论还是属于问题评论的评论，即二级评论
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/4/27 6:48 下午
 */
@Data
public class CommentCreateDTO {
    private Long parentId;
    private String content;
    private Integer type;
}

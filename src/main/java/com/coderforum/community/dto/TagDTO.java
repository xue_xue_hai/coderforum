package com.coderforum.community.dto;

import lombok.Data;

import java.util.List;

/**
 * 用于存储tag标签的数据对象
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/5/6 5:18 下午
 */
@Data
public class TagDTO {
    private String categoryName;
    private List<String> tags;
}

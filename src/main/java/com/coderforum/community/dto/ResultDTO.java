package com.coderforum.community.dto;

import com.coderforum.community.exception.CustomizeErrorCode;
import com.coderforum.community.exception.CustomizeException;
import lombok.Data;


/**
 * 用于返回包含异常代码和异常信息对象的类，其中包括返回正确代码和正确信息，也包括啊异常代码和异常信息
 *
 * @Author: xuexuehai
 * @MailBox: xuehai.xue@qq.com
 * @Date: 2021/4/27 8:25 下午
 */
@Data
public class ResultDTO<T> {
    private Integer code;
    private String message;
    private T data;

    /**
     * 根据异常代码和异常信息返回一个ResultDTO数据传输对象
     * @param code 传入的异常代码
     * @param message 传入的异常信息
     * @return 返回包含异常代码和异常信息的ResultDTO对象
     */
    public static ResultDTO errorOf(Integer code, String message) {
        ResultDTO resultDTO = new ResultDTO();
        resultDTO.setCode(code);
        resultDTO.setMessage(message);
        return resultDTO;
    }

    /**
     * 根据传入的异常代码返回异常代码和异常信息
     * @param errorCode 传入的异常代码
     * @return 返回异常代码和异常代码指定的异常信息
     */
    public static ResultDTO errorOf(CustomizeErrorCode errorCode) {
        return errorOf(errorCode.getCode(), errorCode.getMessage());
    }

    /**
     * 根据传入的异常类型，返回指定的异常代码和异常信息
     *
     * @param e 传入的异常类型
     * @return 返回errorOf
     */
    public static ResultDTO errorOf(CustomizeException e) {
        return errorOf(e.getCode(), e.getMessage());
    }

    /**
     * 当请求成功时返回 code = 200 以及成功信息
     * @return 带有状态码和信息的对象
     */
    public static ResultDTO okOf(){
        ResultDTO resultDTO = new ResultDTO();
        resultDTO.setCode(200);
        resultDTO.setMessage("请求成功");
        return resultDTO;
    }

    /**
     * okOf的泛型方法
     * 当请求成功时返回 code = 200 以及成功信息
     * @return 带有状态码和信息的对象
     */
    public static <T> ResultDTO okOf(T t){
        ResultDTO resultDTO = new ResultDTO();
        resultDTO.setCode(200);
        resultDTO.setMessage("请求成功");
        resultDTO.setData(t);
        return resultDTO;
    }
}
